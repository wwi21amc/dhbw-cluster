resource "hcloud_server" "cluster-server" {
  name        = "dhbw-cluster"
  image       = "ubuntu-20.04"
  server_type = "cx11"
  location    = "nbg1"
  ssh_keys    = [hcloud_ssh_key.joe.name, hcloud_ssh_key.gitlab.name]

  provisioner "file" {
    source      = "setup-cluster.sh"
    destination = "/tmp/setup-cluster.sh"

    connection {
      type        = "ssh"
      user        = "root"
      host        = "${self.ipv4_address}"
      private_key = var.ssh_gitlab_private_key
    }
  }

  provisioner "remote-exec" {
    inline = [
        "chmod +x /tmp/setup-cluster.sh",
        "/tmp/setup-cluster.sh ${var.gitlab_token_docker_pull}",
        "rm /tmp/setup-cluster.sh"
    ]

    connection {
      type        = "ssh"
      user        = "root"
      host        = "${self.ipv4_address}"
      private_key = var.ssh_gitlab_private_key
    }
  }
}

resource "hcloud_ssh_key" "joe" {
  name       = "joe"
  public_key = var.ssh_joe_public_key
}

resource "hcloud_ssh_key" "gitlab" {
  name       = "gitlab"
  public_key = var.ssh_gitlab_public_key
}
