terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.27.2"
    }
  }
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/58249922/terraform/state/default"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
    retry_wait_max = 15
    username       = "terraform"

    # you need to provide the password like this
    # terraform init -backend-config="password=$GITLAB_TERRAFORM_STATE_TOKEN"
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

variable "gitlab_token_docker_pull" {
  sensitive = false # false so that we still see the provisioner logs
}
variable "hcloud_token" {
  sensitive = true
}
variable "ssh_gitlab_private_key" {
  sensitive = true
}
variable "ssh_gitlab_public_key" {
  sensitive = true
}
variable "ssh_joe_public_key" {
  sensitive = true
}
