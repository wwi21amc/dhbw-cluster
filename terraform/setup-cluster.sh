#!/bin/sh

token=$1

apt update
apt -y upgrade
apt-get install -y ca-certificates curl gnupg apt-transport-https

# Setup k3s
## We have to disable traefik (comes out of the box with k3s) because it uses port 80 and blocks nginx-ingress from running
## (nginx-ingress conrtroller cannot get scheduled)
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION="v1.28.4-rc1+k3s1" INSTALL_K3S_EXEC="--disable=traefik" sh -

# To retrieve the kubeconfig
# cp /etc/rancher/k3s/k3s.yaml /tmp/kubeconfig
# pip=$(curl https://ipinfo.io/ip)
# sed -i "s/127.0.0.1/$pip/" /tmp/kubeconfig
# echo "$(cat /tmp/kubeconfig)" | base64 -w 0 > b64config

# Install nginx-ingress
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.2/deploy/static/provider/cloud/deploy.yaml
kubectl patch deployment ingress-nginx-controller -n ingress-nginx --type json -p '[ { "op": "replace", "path": "/spec/template/spec/hostNetwork", "value": true } ]'
## The validation webhook is not reachable from the Internet. Let's disable it
## See https://github.com/kubernetes/ingress-nginx/issues/5401#issuecomment-662424306
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission

kubectl create namespace teamdummy-ns
kubectl create namespace team1-ns
kubectl create namespace team2-ns
kubectl create namespace team3-ns
kubectl create namespace team4-ns
kubectl create namespace team5-ns

# Create image pull secrets for each namespace
kubectl create secret docker-registry gitlab-auth -n teamdummy-ns --docker-server=https://registry.gitlab.com --docker-username=dhbwbot --docker-password="$token"
kubectl create secret docker-registry gitlab-auth -n team1-ns --docker-server=https://registry.gitlab.com --docker-username=dhbwbot --docker-password="$token"
kubectl create secret docker-registry gitlab-auth -n team2-ns --docker-server=https://registry.gitlab.com --docker-username=dhbwbot --docker-password="$token"
kubectl create secret docker-registry gitlab-auth -n team3-ns --docker-server=https://registry.gitlab.com --docker-username=dhbwbot --docker-password="$token"
kubectl create secret docker-registry gitlab-auth -n team4-ns --docker-server=https://registry.gitlab.com --docker-username=dhbwbot --docker-password="$token"
kubectl create secret docker-registry gitlab-auth -n team5-ns --docker-server=https://registry.gitlab.com --docker-username=dhbwbot --docker-password="$token"

# Install cert manager
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.13.2/cert-manager.yaml
