# dhbw-cluster

Forked from [goo-cluster](https://gitlab.com/andary/goo-cluster/).

After the deployment went through:
- Update the DNS record for your domain to point to the VM's Public IP.
- The Terraform stage will fail on the first run. SSH into the machine and extract a kubeconfig to add to this repo's CI/CD variables (see `./terraform/setup-cluster.sh`)

Run this to generate a kubeconfig file for a team:

```
team="teamdummy"

TOKEN=$(kubectl get secret ${team}-sa-token -n ${team}-ns -o jsonpath='{.data.token}' | base64 --decode)
```

With that token, simply replace the user for the kubeconfig file for the teams:

```
users:
- name: default
  user:
    token: <token>
```